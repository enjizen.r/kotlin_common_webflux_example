package com.wanchalerm.tua.common

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinCommonWebfluxExampleApplication

fun main(args: Array<String>) {
	runApplication<KotlinCommonWebfluxExampleApplication>(*args)
}
